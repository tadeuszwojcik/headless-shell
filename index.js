const os = require('os')
const childProcess = require('child_process')
const path = require('path')
console.log('->>',path.resolve('./node_modules/headless-shell/deps/'))
const chrome = childProcess.spawn(
  path.resolve('./node_modules/.bin/headless_shell'),
  [
      '--headless',
      '--no-sandbox',
    '--window-size=1280x1696', // Letter size
    '--disable-gpu',
    '--remote-debugging-port=9222',
    '--user-data-dir=/tmp',
    '--data-path=/tmp/data-path',
    '--homedir=/tmp',
    '--profile-directory=/tmp',
    '--disable-translate',
     '--disable-extensions',
 '--disable-background-networking',
    '--safebrowsing-disable-auto-update',
       '--disable-sync',
          '--metrics-recording-only',
            '--disable-default-apps',
               '--no-first-run',
               '--disable-setuid-sandbox',
    '--disk-cache-dir=/tmp/cache-dir',
    '--enable-logging',
      '--hide-scrollbars',
      '--single-process',
      'http://onet.pl'
  ],
  {
    cwd: os.tmpdir(),
    shell: true,
    detached: true,
    env: {
        LD_LIBRARY_PATH: path.resolve('./node_modules/headless-shell/deps/')
    },
  //  stdio: 'ignore' // if you want to listen to events from stdout or stderr, remove this. However, doing so will cause the Lambda function to never exist correctly despite detached: true and unref(). I dunno why.
    
   }
    
);
chrome.stdout.on('data', function (data) {
  console.log('stdout:',  data);
});
chrome.stderr.on('data', function (data) {
  console.log('stderr: ' +  data);
});
chrome.on('close', function (code) {
  console.log('child process exited with code:',  code);
});
chrome.on('error', function (err) {
  console.log('Failed to start child process.', err);
});
